#!/bin/sh

set -ex

ARCH="amd64"
MIRROR="https://deb.debian.org/debian"

rm -rf cache.tmp
mkdir -p cache.tmp

# this could be done with chdist but we want to keep the dependencies minimal
for SUITE in oldstable stable testing unstable; do
	DIRECTORY="`pwd`/debian-$SUITE-$ARCH"

	export APT_CONFIG=$DIRECTORY/etc/apt/apt.conf

	mkdir -p $DIRECTORY/etc/apt/trusted.gpg.d/
	mkdir -p $DIRECTORY/etc/apt/apt.conf.d/
	mkdir -p $DIRECTORY/etc/apt/sources.list.d/
	mkdir -p $DIRECTORY/etc/apt/preferences.d/
	mkdir -p $DIRECTORY/var/lib/apt/lists/partial/
	mkdir -p $DIRECTORY/var/lib/dpkg/
	mkdir -p $DIRECTORY/var/cache/apt/apt-file/
	mkdir -p $DIRECTORY/var/cache/apt/archives/partial

	# we have to also set Apt::Architectures to avoid foreign architectures
	# from the host influencing this
	cat << END > "$APT_CONFIG"
Apt {
   Architecture "$ARCH";
   Architectures { "$ARCH"; };
};

Dir "$DIRECTORY";
Dir::State::status "$DIRECTORY/var/lib/dpkg/status";
Dir::Etc::Trusted "/etc/apt/trusted.gpg";
Dir::Etc::TrustedParts "/etc/apt/trusted.gpg.d";

Acquire::Check-Valid-Until false;
Acquire::Languages "none";
END

	touch $DIRECTORY/var/lib/dpkg/status

	echo deb $MIRROR $SUITE main > $DIRECTORY/etc/apt/sources.list
	# FIXME: also add updates and security mirrors for stable releases

	grep-aptavail -sPackage,SHA256 -n -FSHA256 -e '^[a-z0-9]{64}$' | paste -sd "  \n" | sort > aptavail-before

	apt-get update -o Acquire::AllowReleaseInfoChange=true -o quiet::ReleaseInfoChange=true

	grep-aptavail -sPackage,SHA256 -n -FSHA256 -e '^[a-z0-9]{64}$' | paste -sd "  \n" | sort > aptavail-after

	mkdir -p "cache.tmp/$SUITE/"

	> force-new-download

	# packages existing before and after with the same checksum just get
	# copied
	comm -12 aptavail-before aptavail-after | cut -d ' ' -f 1 \
		| uniq | while read package; do
			# check if directory of former run exists as expected
			# and otherwise fall back to downloading it
			if [ -d "cache/$SUITE/$package" ]; then
				cp -rl "cache/$SUITE/$package" "cache.tmp/$SUITE/$package"
			else
				echo $package >> force-new-download
			fi
		done

	# packages which are new or have a different checksum or which were
	# made part of the list force-new-download above get newly downloaded
	#
	# the xargs makes sure to run `apt-get download` with multiple
	# packages at a time as it would otherwise be the bottleneck
	{ comm -13 aptavail-before aptavail-after | cut -d ' ' -f 1; cat force-new-download; } \
		| sort -u | xargs apt-get --print-uris download | sort -u \
		| sed -ne "s/^'\([^']\+\)'\s\+\([^_]\+\)_.*/\2 \1/p" \
		| while read package url; do
			mkdir -p "cache.tmp/$SUITE/$package"
			curl --retry 2 --location --silent "$url" \
				| dpkg-deb --ctrl-tarfile /dev/stdin \
				| tar -C "cache.tmp/$SUITE/$package" --exclude=./md5sums -x
		done

done

rm -rf cache csearchindex
mv cache.tmp cache

CSEARCHINDEX=./csearchindex cindex cache

# tarball must be created after cindex was run
for SUITE in oldstable stable testing unstable; do
	XZ_OPT=-9e tar -C "cache/" -cJf "cache/$SUITE.tar.xz" "$SUITE"
done
